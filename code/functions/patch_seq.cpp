// [[Rcpp::depends(RcppArmadillo)]]

#include <RcppArmadilloExtensions/sample.h>
#include <algorithm>
#include <vector>

using namespace Rcpp ;

// [[Rcpp::export]]
NumericVector patch_seq( NumericVector x,
                         int initial,
                         std::vector<int> targets,
                         NumericMatrix trans_mat,
                         int maxStep
)
{
  NumericVector patches;
  NumericVector patch;

  int i=initial;
  int j=0;
  while (std::find(targets.begin(), targets.end(), i) == targets.end() && j<maxStep) {
    NumericVector prob = trans_mat(i-1 , _);
    patch = RcppArmadillo::sample(x, 1, false, prob);
    patches.push_back(patch[0]);
    i = patch[0];
    j++;
  } ;
  return patches ;
}
