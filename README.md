# Predictive multispecies connectivity modelling to support planning and adaptive management of climate resilient Marine Protected Area networks

## Research Team
**Principal Researchers**  
Patrick Thompson (Principal Investigator) - patrick.thompson@dfo-mpo.gc.ca  
Emily Rubidge (Principal Investigator) - emily.rubidge@dfo-mpo.gc.ca  
Sarah Friesen (Principal Analysist) - sarah.friesen@dfo-mpo-gc.ca  
Karen Hunter (Principal Investigator) - karen.hunter@dfo-mpo.gc.ca  

**Collaborators**  
Angelica Peña (DFO Pacific) - Regional Oceanographic Model  
Amber Holdsworth (DFO Pacific) - Regional Oceanographic Model  
Natalie Ban (U. Victoria) - MPA Design, Connectivity, and Climate Impacts  
Ryan Stanley (DFO Maritimes) - MPA Design and Evaluation  

**Client Sector Collaborators**  
Joy Hillier - Oceans Management  
Jason Dunham - Integrated Oceans Management  

## Overview

Through Canada’s commitment to marine conservation, Marine Protected Area (MPA) networks are being developed in five priority bioregions. Ecological connectivity—the exchange of nutrients, individuals, or genes—is a key element for designing resilient MPA networks, however its estimation is challenged by the very diversity networks are trying to protect.  Here, we propose to estimate multispecies connectivity for fish communities using circuit theory (FS-06). Circuit theory analyses offer the opportunity to assess the connectivity of MPA networks and to identify barriers to connectivity<sup>1</sup>. We will build on previous work<sup>2</sup>to develop a modelling framework that predicts habitat connectivity for diverse species assemblages based on 1) habitat suitability, 2) ocean currents, and 3) climatic conditions. This innovative approach integrates circuit theory analyses with existing outputs from spatial fish community models and oceanographic models (AEb-01). We will develop this method using the adult stage of 45 benthic fish species across the Northern and Southern Shelf Bioregions using contemporary and future climate projections (AEb-07), complementing ongoing research on larval connectivity. Thus, we will provide quantitative estimates of connectivity that can inform the design and adaptive management of a MPA network that preserves a connected and diverse fish community in a changing climate (AEb-01).

## Objectives


- Develop multispecies connectivity modelling framework and code that can be adapted and applied across regions and taxonomic groups.   
- Apply the framework to model habitat connectivity for 45 benthic fish species in the Northern and Southern Bioregions.  
- Identify areas that contribute disproportionately to benthic habitat connectivity across the region.  
- Evaluate multispecies connectivity between existing MPAs and highlight priority areas for additional MPAs to improve connectivity in the future  
- Predict how multispecies connectivity will change under projected future conditions.

## Methods
We will develop a framework that applies the Circuitscape connectivity analysis algorithm<sup>1,3</sup> based on inputs from joint species distribution models and oceanographic models to make multispecies predictions about habitat connectivity. Circuitscape uses circuit theory to emulate potential movement routes of organisms. The software considers landscapes, or seascapes in our case, as conductive surfaces connected by resistors and uses electrical current to simulate random exploratory movements. It considers all possible pathways between pairwise locations, identifying the probable pathways as those with lowest resistance to flow. The result is a map of probabilistic flow across all possible routes.
We will demonstrate this approach using the British Columbia Continental Margin (BCCM) regional ocean model and 45 benthic fish species across the Northern and Southern Shelf Bioregions for which we have environmental suitability models and biomass predictions from a joint species distribution model<sup>4</sup>.
Our analysis will integrate two factors into the resistance surface:
1.	Species-specific habitat and environmental suitability based on depth, substrate, temperature, dissolved oxygen, and primary production. These have been quantified using a joint species distribution model4, based on environmental predictors described in Nephin et al.<sup>5</sup> and outputs from the BCCM model<sup>6</sup>. We will model resistance as inversely proportional to species’ predicted occurrence in each 3 km pixel.
2.	Ocean circulation from the BCCM model6. Resistance will be calculated based on direction of movement relative to currents<sup>7</sup>. 

Flow from each source pixel will be scaled to predicted biomass from Thompson et al.<sup>4</sup> to better represent simulated connectivity patterns. Flow between pixels will be constrained to cells that fall within literature-based species-specific adult movement distances<sup>8</sup>.
We will run this analysis with contemporary climatic conditions (2003-2018), and for projected conditions (2041-2060) under RCP8.5 climate scenario from the BCCM6 to assess how connectivity patterns may change with climate change. 
For each pixel, we will quantify the net community connectivity index (NCCI) as the summed electrical current flow across all species<sup>9</sup>. A habitat isolation index (HII) will be calculated as the sum of the total effective resistance encountered by flow leaving each source pixel, both for each species and for the community. To account for spatial differences in species richness, both indices will be scaled by the predicted species richness from Thompson et al.<sup>4</sup> 
High NCCI values will identify areas that are important for multispecies connectivity. We will use spatial overlay analysis to evaluate the extent to which existing MPAs capture areas with high NCCI values relative to their footprint within the region and determine which MPAs are connected to each other. Connectivity patterns will be compared between time periods to assess potential climate impacts. 

### Work plan

Year 1  
- 	Write code to perform analysis under contemporary climates  
- 	Run contemporary connectivity analysis  

Year 2  
-   Adapt code and run analysis under projected 2041-2060 climate  

Year 3  
- 	Write scientific manuscript  
-	Compile results, document data outputs, and submit to data portals  
-	Create Shiny App to visualize results  
-	Write plain language report  

##### Style Guide
http://adv-r.had.co.nz/Style.html

## Citations

1.	McRae, B. H., Dickson, B. G., Keitt, T. H. & Shah, V. B. Using circuit theory to model connectivity in ecology, evolution, and conservation. Ecology 89, 2712–2724 (2008).
2.	Friesen, S. K. et al. Effects of changing ocean temperatures on ecological connectivity among marine protected areas in northern British Columbia. In Prep.
3.	Anantharaman, R., Hall, K., Shah, V. B. & Edelman, A. Circuitscape in Julia: High Performance Connectivity Modelling to Support Conservation Decisions. JuliaCon Proc. 1, 58 (2020).
4.	Thompson, P. L. et al. Spatiotemporal Changes in Groundfish Biodiversity and Composition in Canadian Northern Pacific Waters. In Prep.
5.	Nephin, J., Gregr, E. J., Germain, C. S., Fields, C. & Finney, J. L. Development of a Species Distribution Modelling Framework and its Application to Twelve Species on Canada’s Pacific Coast. DFO Can Sci Advis Sec Res Doc 2020/004, xii + 107 p (2020).
6.	Peña, M. A., Fine, I. & Callendar, W. Interannual variability in primary production and shelf-offshore transport of nutrients along the northeast Pacific Ocean margin. Deep Sea Res. Part II Top. Stud. Oceanogr. 169–170, 104637 (2019).
7.	Dambach, J., Raupach, M. J., Leese, F., Schwarzer, J. & Engler, J. O. Ocean currents determine functional connectivity in an Antarctic deep-sea shrimp. Mar. Ecol. 37, 1336–1344 (2016).
8.	Burt, J. M. et al. Marine protected area network design features that support resilient human-ocean systems: Applications for British Columbia, Canada. https://osf.io/9tdhv (2014) doi:10.31230/osf.io/9tdhv.
9.	Lawler, J. J., Ruesch, A. S., Olden, J. D. & McRae, B. H. Projected climate-driven faunal movement routes. Ecol. Lett. 16, 1014–1022 (2013).
10.	Wilcox, M. et al. Development of spatial connectivity network models in support of spatial conservation planning. In Prep.
